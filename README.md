# SwipeBack
**本项目是基于开源项目SwipeBack进行鸿蒙化的移植和开发的，可以通过项目标签以及github地址（https://github.com/liuguangqiang/SwipeBack）追踪到原安卓项目版本**

#### 项目介绍

SwipeBack is a library that can finish an ability by using gesture.

You can set the swipe direction,such as left,top,right and bottom.

SwipeBack是一个库，可以使用手势完成活动。
您可以设置滑动方向，例如左，上，右和下

## Usage

### Gradle
```
添加库的依赖
方式一：
添加har包到lib文件夹内
在entry的gradle内添加如下代码
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])

方式二：
allprojects {
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:SwipeBack:1.0.1'
```

```
### SwipeBackActivity
SwipeBackActivity is a base activity for using this library more easier.


```
public class CommonAbility extends SwipeBackActivity {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        ComponentContainer componentContainer1 = (ComponentContainer) LayoutScatter
                .getInstance(this)
                .parse(ResourceTable.Layout_ability_common, null, false);
        super.setUIContent(componentContainer1);
        setDragEdge(SwipeBackLayout.DragEdge.LEFT);
    }
}
```
public class DemoAbility extends Ability {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_demo);
        SwipeBackLayout seipelayout = (SwipeBackLayout) findComponentById(ResourceTable.Id_swipebacklayout);
        seipelayout.setOnFinishListener(new SwipeBackLayout.OnFinishListener() {
            @Override
            public void onFinishState() {
                terminateAbility();
            }
        });
    }
}

```
### Layout and activity theme.


```

   <com.liuguangqiang.swipeback.SwipeBackLayout
           ohos:id="$+id:swipebacklayout"
           ohos:height="match_parent"
           ohos:width="match_parent">

    <!-- SwipeBackLayout must contains only one direct child -->

   </com.liuguangqiang.swipeback.SwipeBackLayout>

```
```

### Swipe Direction
You can set  DragEdge to change the swipe direction.

```
 public enum DragEdge {
      	LEFT,

        TOP,

        RIGHT,

        BOTTOM
    }
```

### Support Views
SwipeBackLayout must contains only one direct child.

Such as:

* LinearLayout,
* RelativeLayout,
* ListView,
* ViewPager
* WebView

## License

    Copyright 2015 Eric Liu

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

