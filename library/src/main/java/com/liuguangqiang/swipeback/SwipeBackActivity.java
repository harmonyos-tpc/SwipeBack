package com.liuguangqiang.swipeback;

import com.liuguangqiang.swipeback.library.ResourceTable;

import ohos.aafwk.ability.Ability;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;

/**
 * Created by Eric on 15/3/3.
 */
public class SwipeBackActivity extends Ability {
    private static SwipeBackLayout.DragEdge DEFAULT_DRAG_EDGE = SwipeBackLayout.DragEdge.LEFT;
    private SwipeBackLayout swipeBackLayout;
    private float mDownTouchX;
    private float mDownTouchY;

    @Override
    public void setUIContent(ComponentContainer componentContainer) {
        super.setUIContent(getContainer());

        swipeBackLayout.addComponent(componentContainer);

        swipeBackLayout.setOnFinishListener(new SwipeBackLayout.OnFinishListener() {
            @Override
            public void onFinishState() {
                terminateAbility();
            }
        });
    }

    private ComponentContainer getContainer() {
        DependentLayout container = new DependentLayout(this);
        swipeBackLayout = new SwipeBackLayout(this);
        swipeBackLayout.setDragEdge(DEFAULT_DRAG_EDGE);
        Image image = new Image(this);
        ShapeElement element = new ShapeElement();
        element.setRgbColor(new RgbColor(Color.BLACK.getValue()));

        ComponentContainer.LayoutConfig config = new StackLayout.LayoutConfig(
                ComponentContainer.LayoutConfig.MATCH_PARENT,
                ComponentContainer.LayoutConfig.MATCH_PARENT);
        image.setBackground(element);
        image.setScaleMode(Image.ScaleMode.CLIP_CENTER);
        image.setPixelMap(ResourceTable.Media_test2);
        container.addComponent(swipeBackLayout);
        return container;
    }

    /**
     * 设置滑的方向
     *
     * @param dragEdge 方向
     */
    public void setDragEdge(SwipeBackLayout.DragEdge dragEdge) {
        DEFAULT_DRAG_EDGE = dragEdge;
        swipeBackLayout.setDragEdge(dragEdge);
    }

    /**
     * 设置是否能滑动
     *
     * @param enable 是否能滑动
     */
    public void setEnableSwipe(boolean enable) {
        swipeBackLayout.setEnableSwipe(enable);
    }

    /**
     * 获取布局 SwipeBackLayout
     *
     * @return 布局
     */
    public SwipeBackLayout getSwipeBackLayout() {
        return swipeBackLayout;
    }

    /**
     * 关闭界面
     * @param onFinishListener onFinishListener
     */
    public void setOnFinishListener(SwipeBackLayout.OnFinishListener onFinishListener){
        swipeBackLayout.setOnFinishListener(onFinishListener);
    }

}
