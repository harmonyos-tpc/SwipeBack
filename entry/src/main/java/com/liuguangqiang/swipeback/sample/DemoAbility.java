package com.liuguangqiang.swipeback.sample;

import com.liuguangqiang.swipeback.SwipeBackLayout;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * 向下滑界面
 */
public class DemoAbility extends Ability {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_demo);
        SwipeBackLayout seipelayout = (SwipeBackLayout) findComponentById(ResourceTable.Id_swipebacklayout);
        seipelayout.setOnFinishListener(new SwipeBackLayout.OnFinishListener() {
            @Override
            public void onFinishState() {
                terminateAbility();
            }
        });
    }
}
