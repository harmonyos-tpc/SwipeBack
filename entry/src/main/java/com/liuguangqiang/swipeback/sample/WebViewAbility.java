package com.liuguangqiang.swipeback.sample;

import com.liuguangqiang.swipeback.SwipeBackActivity;
import com.liuguangqiang.swipeback.SwipeBackLayout;

import ohos.aafwk.content.Intent;

import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.webengine.WebView;

/**
 * WebView界面
 */
public class WebViewAbility extends SwipeBackActivity {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        ComponentContainer componentContainer1 = (ComponentContainer) LayoutScatter
                .getInstance(this)
                .parse(ResourceTable.Layout_ability_webview, null, false);
        super.setUIContent(componentContainer1);
        setDragEdge(SwipeBackLayout.DragEdge.TOP);
        WebView webView = (WebView) findComponentById(ResourceTable.Id_webview);
        webView.getWebConfig().setJavaScriptPermit(true);  // 如果网页需要使用JavaScript，增加此行；如何使用JavaScript下文有详细介绍
        final String url = "https://github.com"; // EXAMPLE_URL由开发者自定义
        webView.load(url);
    }

}
