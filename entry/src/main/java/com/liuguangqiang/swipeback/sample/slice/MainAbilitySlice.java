/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.liuguangqiang.swipeback.sample.slice;

import com.liuguangqiang.swipeback.SwipeBackLayout;

import com.liuguangqiang.swipeback.sample.ResourceTable;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

/**
 * 主界面
 */
public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener,
        SwipeBackLayout.OnFinishListener {
    private Button demo;
    private Button common;
    private Button listview;
    private Button viewpage;
    private Button webview;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initUI();
    }

    private void initUI() {
        common = (Button) findComponentById(ResourceTable.Id_btn_common);
        demo = (Button) findComponentById(ResourceTable.Id_btn_demo);
        listview = (Button) findComponentById(ResourceTable.Id_btn_ListView);
        viewpage = (Button) findComponentById(ResourceTable.Id_btn_viewpage);
        webview = (Button) findComponentById(ResourceTable.Id_btn_webview);

        common.setClickedListener(this);
        demo.setClickedListener(this);
        listview.setClickedListener(this);
        viewpage.setClickedListener(this);
        webview.setClickedListener(this);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_btn_common:
                startAllAbiility("com.liuguangqiang.swipeback.sample.CommonAbility");
                break;
            case ResourceTable.Id_btn_demo:
                startAllAbiility("com.liuguangqiang.swipeback.sample.DemoAbility");
                break;
            case ResourceTable.Id_btn_ListView:
                startAllAbiility("com.liuguangqiang.swipeback.sample.ListViewAbility");
                break;
            case ResourceTable.Id_btn_viewpage:
                startAllAbiility("com.liuguangqiang.swipeback.sample.ViewPagerAbility");
                break;
            case ResourceTable.Id_btn_webview:
                startAllAbiility("com.liuguangqiang.swipeback.sample.WebViewAbility");
                break;
            default:
                break;
        }
    }

    private void startAllAbiility(String ability) {
        Intent secondIntent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("com.mykey.myapps")
                .withAbilityName(ability)
                .build();
        secondIntent.setOperation(operation);
        startAbility(secondIntent);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onFinishState() {
    }
}
