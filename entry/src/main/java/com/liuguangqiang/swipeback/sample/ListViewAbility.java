package com.liuguangqiang.swipeback.sample;

import com.liuguangqiang.swipeback.SwipeBackActivity;
import com.liuguangqiang.swipeback.SwipeBackLayout;

import ohos.aafwk.content.Intent;

import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * Listview界面
 */
public class ListViewAbility extends SwipeBackActivity {
    private List<String> list = new ArrayList<>();
    private MyAdapter adapter;
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        ComponentContainer componentContainer1 = (ComponentContainer) LayoutScatter
                .getInstance(this)
                .parse(ResourceTable.Layout_ability_listview, null, false);
        super.setUIContent(componentContainer1);
        setDragEdge(SwipeBackLayout.DragEdge.LEFT);

        for (int i = 0; i < 50; i++) {
            list.add("Test" + i);
        }
        ListContainer listview = (ListContainer) componentContainer1.findComponentById(ResourceTable.Id_listview);
        adapter = new MyAdapter(this,list);
        listview.setItemProvider(adapter);



    }


    /**
     * MyAdapter
     */
    class MyAdapter extends BaseItemProvider {
        private Context mContext;
        private List<String> mDataList;

        MyAdapter(Context context,List<String> list) {
            this.mDataList = list;
            mContext = context;
        }


        @Override
        public int getCount() {
            return mDataList == null ? 0 : mDataList.size();
        }

        @Override
        public String getItem(int position) {
            return mDataList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Component getComponent(int position, Component convertView, ComponentContainer container) {
            ViewHolder holder = null;
            if (convertView == null) {
                convertView = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_item, container, false);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            Text text = (Text) holder.getItemView().findComponentById(ResourceTable.Id_helloText);
            text.setText(mDataList.get(position));
            return convertView;
        }

        /**
         * ViewHolder
         */
        public class ViewHolder {
            private final Component itemView;

            ViewHolder(Component itemView) {
                if (itemView == null) {
                    throw new IllegalArgumentException("itemView may not be null");
                } else {
                    this.itemView = itemView;
                }
            }

            /**
             * getItemView
             * @return Component
             */
            public Component getItemView() {
                return itemView;
            }
        }
    }

}
