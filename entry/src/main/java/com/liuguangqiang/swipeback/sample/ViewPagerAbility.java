package com.liuguangqiang.swipeback.sample;

import com.liuguangqiang.swipeback.SwipeBackActivity;
import com.liuguangqiang.swipeback.SwipeBackLayout;

import ohos.aafwk.content.Intent;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;

import java.util.ArrayList;
import java.util.List;

/**
 * ViewPager界面
 */
public class ViewPagerAbility extends SwipeBackActivity {
    private PageSlider pageSlider;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        ComponentContainer componentContainer1 = (ComponentContainer) LayoutScatter
                .getInstance(this)
                .parse(ResourceTable.Layout_ability_viewpager, null, false);
        super.setUIContent(componentContainer1);

        pageSlider = (PageSlider) componentContainer1.findComponentById(ResourceTable.Id_viewpager_demo);
        TestPagerProvider testPagerProvider = new TestPagerProvider(getData());

        setDragEdge(SwipeBackLayout.DragEdge.LEFT);
        pageSlider.setProvider(testPagerProvider);
        pageSlider.addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int poisition, float vs, int i1) {
            }

            @Override
            public void onPageSlideStateChanged(int poisition) {
            }

            @Override
            public void onPageChosen(int poisition) {
                if (poisition == 0) {
                    setEnableSwipe(true);
                } else {
                    setEnableSwipe(false);
                }
            }
        });
    }

    private ArrayList<DataItem> getData() {
        ArrayList<DataItem> dataItems = new ArrayList<>();
        dataItems.add(new DataItem("Page A"));
        dataItems.add(new DataItem("Page B"));
        dataItems.add(new DataItem("Page C"));
        dataItems.add(new DataItem("Page D"));
        dataItems.add(new DataItem("Page E"));
        dataItems.add(new DataItem("Page F"));
        dataItems.add(new DataItem("Page G"));

        return dataItems;
    }


    class TestPagerProvider extends PageSliderProvider {
        private List<DataItem> list;

        TestPagerProvider(List<DataItem> list) {
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object createPageInContainer(ComponentContainer componentContainer, int poisition) {
            final DataItem data = list.get(poisition);
            Text label = new Text(null);
            label.setTextAlignment(TextAlignment.CENTER);
            label.setLayoutConfig(
                    new StackLayout.LayoutConfig(
                            ComponentContainer.LayoutConfig.MATCH_PARENT,
                            ComponentContainer.LayoutConfig.MATCH_PARENT
                    )
            );

            label.setText(data.mText);
            label.setTextColor(Color.BLACK);
            label.setTextSize(50);
            ShapeElement element = new ShapeElement();
            element.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#AFEEEE")));
            label.setBackground(element);
            componentContainer.addComponent(label);

            return label;
        }

        @Override
        public void destroyPageFromContainer(ComponentContainer componentContainer, int poisition, Object obj) {
            componentContainer.removeComponent((Component) obj);
        }

        @Override
        public boolean isPageMatchToObject(Component component, Object obj) {
            return true;
        }


    }

    /**
     * 数据体
     */
    public static class DataItem {
        String mText;

        public DataItem(String txt) {
            mText = txt;
        }
    }
}
