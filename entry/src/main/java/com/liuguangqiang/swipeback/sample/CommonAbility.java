package com.liuguangqiang.swipeback.sample;

import com.liuguangqiang.swipeback.SwipeBackActivity;
import com.liuguangqiang.swipeback.SwipeBackLayout;

import ohos.aafwk.content.Intent;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

/**
 * 向左滑界面
 */
public class CommonAbility extends SwipeBackActivity {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        ComponentContainer componentContainer1 = (ComponentContainer) LayoutScatter
                .getInstance(this)
                .parse(ResourceTable.Layout_ability_common, null, false);
        super.setUIContent(componentContainer1);

        setDragEdge(SwipeBackLayout.DragEdge.LEFT);
    }
}
